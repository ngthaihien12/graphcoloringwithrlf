package com.sam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    static List<BookingRequest> requestList = new ArrayList<>();

    public static void main(String[] args) {
	// write your code here
        String vertext4 = "4vertex.txt";
        String vertext8 = "8vertex.txt";
        String vertext10 = "10vertex.txt";
        String vertext11 = "11vertex.txt";
        String vertext12 = "12vertex.txt";
        String vertext13 = "13vertex.txt";
        //initial graph from file
//        int[][] graph = initialGraph(vertext13);
        //initial graph from list booking
        initBooking();
        int[][] graph = initialGraphFromBooking(requestList);
        int[] color = new int[graph.length];
        System.out.println("Number of vertex: "+ graph.length);
        System.out.println("Start graph coloring with RLF: " + java.time.LocalDateTime.now());
        RLFAlgorithm rlfAlgorithm = new RLFAlgorithm(graph, color);
        rlfAlgorithm.graphColoring();
    }

    private static void initBooking(){
        BookingRequest b1 = new BookingRequest();
        BookingRequest b2 = new BookingRequest();
        BookingRequest b3 = new BookingRequest();
        BookingRequest b4 = new BookingRequest();
        BookingRequest b5 = new BookingRequest();
        BookingRequest b6 = new BookingRequest();
        BookingRequest b7 = new BookingRequest();
        BookingRequest b8 = new BookingRequest();
        BookingRequest b9 = new BookingRequest();
        BookingRequest b10 = new BookingRequest();
        BookingRequest b11 = new BookingRequest();
        BookingRequest b12 = new BookingRequest();
        BookingRequest b13 = new BookingRequest();
        b1.setId(1);
        String from1 = "2019-11-08 00:00:00";
        String to1 = "2019-11-09 00:00:00";
        b1.setFrom(Utils.convertStringToTimestamp(from1));
        b1.setTo(Utils.convertStringToTimestamp(to1));
        //booking request2
        b2.setId(2);
        String from2 = "2019-11-03 00:00:00";
        String to2 = "2019-11-04 00:00:00";
        b2.setFrom(Utils.convertStringToTimestamp(from2));
        b2.setTo(Utils.convertStringToTimestamp(to2));
        //booking request3
        b3.setId(3);
        String from3 = "2019-11-05 00:00:00";
        String to3 = "2019-11-06 00:00:00";
        b3.setFrom(Utils.convertStringToTimestamp(from3));
        b3.setTo(Utils.convertStringToTimestamp(to3));
        //booking request4
        b4.setId(4);
        String from4 = "2019-11-01 00:00:00";
        String to4 = "2019-11-02 00:00:00";
        b4.setFrom(Utils.convertStringToTimestamp(from4));
        b4.setTo(Utils.convertStringToTimestamp(to4));
        //booking request5
        b5.setId(5);
        String from5 = "2019-11-01 00:00:00";
        String to5 = "2019-11-10 00:00:00";
        b5.setFrom(Utils.convertStringToTimestamp(from5));
        b5.setTo(Utils.convertStringToTimestamp(to5));
        //booking request6
        b6.setId(6);
        String from6 = "2019-11-03 00:00:00";
        String to6 = "2019-11-08 00:00:00";
        b6.setFrom(Utils.convertStringToTimestamp(from6));
        b6.setTo(Utils.convertStringToTimestamp(to6));
        //booking request7
        b7.setId(7);
        String from7 = "2019-11-09 00:00:00";
        String to7 = "2019-11-12 00:00:00";
        b7.setFrom(Utils.convertStringToTimestamp(from7));
        b7.setTo(Utils.convertStringToTimestamp(to7));
        //booking request8
        b8.setId(8);
        String from8 = "2019-11-06 00:00:00";
        String to8 = "2019-11-07 00:00:00";
        b8.setFrom(Utils.convertStringToTimestamp(from8));
        b8.setTo(Utils.convertStringToTimestamp(to8));
        //booking request with color 0
        b9.setId(9);
        String from9 = "2019-11-04 00:00:00";
        String to9 = "2019-11-05 00:00:00";
        b9.setFrom(Utils.convertStringToTimestamp(from9));
        b9.setTo(Utils.convertStringToTimestamp(to9));
        b9.setResourceId(1);
        b10.setId(10);
        String from10 = "2019-11-10 00:00:00";
        String to10 = "2019-11-12 00:00:00";
        b10.setFrom(Utils.convertStringToTimestamp(from10));
        b10.setTo(Utils.convertStringToTimestamp(to10));
        b10.setResourceId(2);
        //booking 11
        b11.setId(11);
        String fromFake = "2019-11-01 00:00:00";
        String toFake = "2019-11-02 00:00:00";
        b11.setFrom(Utils.convertStringToTimestamp(fromFake));
        b11.setTo(Utils.convertStringToTimestamp(toFake));
        b11.setResourceId(1);
        //booking 12
        b12.setId(12);
        String from12 = "2019-11-05 00:00:00";
        String to12 = "2019-11-08 00:00:00";
        b12.setFrom(Utils.convertStringToTimestamp(from12));
        b12.setTo(Utils.convertStringToTimestamp(to12));
        b12.setResourceId(1);
        //booking 13
        b13.setId(13);
        String from13 = "2019-11-10 00:00:00";
        String to13 = "2019-11-12 00:00:00";
        b13.setFrom(Utils.convertStringToTimestamp(from13));
        b13.setTo(Utils.convertStringToTimestamp(to13));
        b13.setResourceId(1);
        for (int i= 0; i< 575; i++){
            requestList.add(b1);
            requestList.add(b2);
            requestList.add(b3);
            requestList.add(b4);
            requestList.add(b5);
            requestList.add(b6);
            requestList.add(b7);
            requestList.add(b8);
            requestList.add(b9);
            requestList.add(b10);
        }
    }

    private static int[][] initialGraph(String filePath){
        try {
            File file = new File(filePath);
            BufferedReader br = null;
            br = new BufferedReader(new FileReader(file));
            String st;
            int n = 0;
            int[][] graph = new int[100][100];
            while (((st = br.readLine()) != null)) {
                String tmp[] = st.split(" ");
                for (int j = 0; j < tmp.length; j++) {
                    graph[n][j] = Integer.parseInt(tmp[j]);
                }
                n++;
            }
            int[][] graphColoring = new int[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    graphColoring[i][j] = graph[i][j];
                }
            }
            System.out.println("---***---GraphColor---***---");
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    System.out.print(graphColoring[i][j] + "\t");
                }
                System.out.println("\n");
            }
            return graphColoring;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private static int[][] initialGraphFromBooking(List<BookingRequest> requestList) {
        int size= requestList.size();
        int[][] graph = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j) {
                    graph[i][j] = 0;
                } else {
                    if ((Utils.isInTime(requestList.get(i).getFrom(), requestList.get(i).getTo(), requestList.get(j).getFrom(), requestList.get(j).getTo())) ||
                            (Utils.isInTime(requestList.get(j).getFrom(), requestList.get(j).getTo(), requestList.get(i).getFrom(), requestList.get(i).getTo()))) {
                        graph[i][j] = 1;
                    } else {
                        graph[i][j] = 0;
                    }
                }
            }
        }
//        System.out.println("---***---Graph---***---");
//        for (int i = 0; i < graph.length; i++) {
//            for (int j = 0; j < graph.length; j++) {
//                System.out.print(graph[i][j] + "\t");
//            }
//            System.out.println("\n");
//        }
        return graph;
    }
}
