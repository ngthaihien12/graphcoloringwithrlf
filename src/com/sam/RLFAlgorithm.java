package com.sam;

public class RLFAlgorithm {
    private int n;
    private int[][] graph;
    private int color[];
    //mảng chứa bậc của mỗi đỉnh trong đồ thị
    private int[] degree;
    //mảng dùng để chứa các đỉnh ko kề với đỉnh đang xét, non-neighbors
    private int[] NN;
    //biến đếm số đỉnh ko kề vs đỉnh đang xét
    private int countNN;
    //biến đêm số đỉnh chưa dc tô màu
    private int unprocessed;
    //biến đếm số đỉnh đã có màu, resource
    private int countHaveResource=0;

    public RLFAlgorithm(int[][] graph, int color[]){
        this.graph= graph;
        this.color= color;
        init();
    }

    private void init() {
        n= graph.length;
        degree = new int[n];
        NN = new int[n];
        for (int i = 0; i < n; i++) {
            if(color[i]!= 0){
                countHaveResource++;
            }
            degree[i] = 0;
            for (int j = 0; j < n; j++)
                //đếm bậc của đỉnh
                if (graph[i][j] == 1)
                    degree[i]++;
        }
        countNN = 0;
        unprocessed = n;
    }

    //nếu có đỉnh đã dc tô màu thì xét đỉnh đó trc, nếu ko thì tìm đỉnh có bậc cao nhất trong số đỉnh chưa dc tô màu
    private int maxDegreeVertex() {
        int max = -1;
        int max_i = 0;
        for (int i = 0; i < n; i++)
            if (color[i] == 0) {
                if (degree[i] > max) {
                    max = degree[i];
                    max_i = i;
                }
            }else {
                if(countHaveResource>0){
                    max_i = i;
                    countHaveResource--;
                    return max_i;
                }
            }
        return max_i;
    }

    // this function is for UpdateNN function
    // it reset the value of scanned array
    private void scannedInit(int[] scanned) {
        for (int i = 0; i < n; i++)
            scanned[i] = 0;
    }

    // this function updates NN array
    private void updateNN(int ColorNumber) {
        countNN = 0;
        // firstly, add all the uncolored vertices into NN set
        for (int i = 0; i < n; i++)
            if (color[i] == 0) {
                NN[countNN] = i;
                countNN++; // when we add a vertex, increase the NNCount
            }
        // then, remove all the vertices in NN that
        // is adjacent to the vertices colored ColorNumber
        for (int i = 0; i < n; i++)
            if (color[i] == ColorNumber) // find one vertex colored ColorNumber
                for (int j = 0; j < countNN; j++)
                    while (graph[i][NN[j]] == 1 && countNN >=1)
                    // remove vertex that adjacent to the found vertex
                    {
                        NN[j] = NN[countNN - 1];
                        countNN--; // decrease the NNCount
                    }
    }

    // this function will find suitable y from NN
    private int findY(int colorNumber, int vertexInCommon) {
        int temp, tmpY, y = 0;
        int[] scanned = new int[n];
        vertexInCommon = 0;
        for (int i = 0; i < countNN; i++) // check the i-th vertex in NN
        {
            tmpY = NN[i];
            temp = 0;
            scannedInit(scanned);
            for (int x = 0; x < n; x++)
                if (color[x] == colorNumber)
                    for (int k = 0; k < n; k++)
                        if (color[k] == 0 && scanned[k] == 0)
                            if (graph[x][k] == 1 && graph[tmpY][k] == 1)
                            {
                                temp++;
                                scanned[k] = 1;
                            }
            if (temp > vertexInCommon) {
                vertexInCommon = temp;
                y = tmpY;
            }
        }
        return y;
    }

    //tìm ra đỉnh có bậc cao nhất trong tập hợp NN, ko kề vs đỉnh đang xét
    private int maxDegreeInNN() {
        int tmpY = 0;
        int temp, max = 0;
        for (int i = 0; i < countNN; i++) {
            temp = 0;
            for (int j = 0; j < n; j++)
                if (color[j] == 0 && graph[NN[i]][j] == 1)
                    temp++;
            if (temp > max) {
                max = temp;
                tmpY = NN[i];
            }
        }
        if (max == 0)
            return NN[0];
        else
            return tmpY;
    }

    //in kết quả tô màu đồ thị
    private void printSolution(int color[]) {
        System.out.println("Graph coloring finish: " + java.time.LocalDateTime.now());
        System.out.println("Following are the assigned colors:");
        for (int i = 0; i < n; i++)
            System.out.println("Vertex " + (i+1) + " use color " + color[i] + " ");
        System.out.println("Number of color: "+ countColor(color, color.length));
    }

    public int[] graphColoring() {
        //x là đỉnh dc xét để tô màu
        //y là đỉnh dc xét tô màu trong tập hợp đỉnh ko kề với x
        int x, y;
        int colorNumber = 0;
        int vertexInCommon = 0;
        while (unprocessed > 0) {
            x = maxDegreeVertex();
            colorNumber++;
            if(color[x]==0)
            color[x] = colorNumber;
            unprocessed--;
            //cập nhật lại tập hợp đỉnh ko kề với đỉnh đang xét
            updateNN(colorNumber);
            while (countNN > 0) {
                y = findY(colorNumber, vertexInCommon);
                if (vertexInCommon == 0)
                    y = maxDegreeInNN();
                if(color[y]==0)
                color[y] = colorNumber;
                unprocessed--;
                updateNN(colorNumber);
            }
        }
        printSolution(color);
        return color;
    }

    private int countColor(int[] color, int n) {
        int res = 1;

        // Pick all elements one by one
        for (int i = 1; i < n; i++)
        {
            int j =0;
            for (j = 0; j < i; j++)
                if (color[i] == color[j])
                    break;

            // If not printed earlier,
            // then print it
            if (i == j)
                res++;
        }
        return res;
    }
}
