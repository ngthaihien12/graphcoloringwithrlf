package com.sam;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static Timestamp convertStringToTimestamp(String data){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parsedDate = dateFormat.parse(data);
            return new Timestamp(parsedDate.getTime());
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isEqualOrBefore(Timestamp time1, Timestamp time2) {
        if (time1.equals(time2) || time1.before(time2)) {
            return true;
        }
        return false;
    }

    //Compare two time
    public static boolean isEqualOrAfter(Timestamp time1, Timestamp time2) {
        if (time1.equals(time2) || time1.after(time2)) {
            return true;
        }
        return false;
    }

    public static boolean isInTime(Timestamp from1, Timestamp to1, Timestamp from2, Timestamp to2){
        return ((isEqualOrAfter(from2, from1)&& from2.before(to1)) || (isEqualOrAfter(to2, from1)&& isEqualOrBefore(to2, to1)) );
    }
}
